---
title: 'Prevalence of Rotavirus in diarrhoea '
output:
  pdf_document: 
  toc : yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{booktabs}
- \usepackage{longtable}
- \usepackage{pdflscape}
- \usepackage{makecell}
- \newcommand{\blandscape}{\begin{landscape}}
- \newcommand{\elandscape}{\end{landscape}}
---
\addtolength{\headheight}{1.0cm} 
\pagestyle{fancyplain} 
\lhead{\includegraphics[height=1.5cm]{cermel-logo}}
\renewcommand{\headrulewidth}{0pt} 


```{r, echo=FALSE, message=FALSE, warning=FALSE}

library(readxl)
library(dplyr, warn.conflicts = FALSE)
library(stringr)
library(knitr)
library(forcats)
library(ggplot2)
library(finalfit)
library(tidyr)
options(kableExtra.latex.load_packages = FALSE)
library(kableExtra)



```


```{r,echo=FALSE, message=FALSE, warning=FALSE}

rotcase <- read_excel("data/Rota15.xlsx", sheet = "case") %>%
      mutate(
    age_group = cut(age, breaks=c(0, 7, 13,19,25, 60), right = FALSE),
    rotapcr= case_when(rotapcr == 0 ~ "neg",
                       rotapcr == 1 ~"pos")) 

rotcont <- read_excel("data/Rota15.xlsx", sheet = "control") %>%
      mutate(
    age_group = cut(age, breaks=c(0, 7, 13,19,25, 60), right = FALSE))

tab1 <- rotcase %>%
      pivot_longer(c(age_group, sex, region)) %>%
      count(name, value, rotapcr)  %>%
     group_by(value) %>% 
     mutate(perc = round(n*100/sum(n),1),
            deno = sum(n),
       tot = paste0(n,"/",deno, " (", perc, ")")) %>%
     filter(rotapcr != "neg") %>%
     select(Characteristics  = value,tot) %>%
   arrange(match(Characteristics, c("[0,7)","[7,13)","[13,19)","[19,25)",
                 "[25,60)","F","M","rural","urban")))

tab2 <- rotcont %>%
      pivot_longer(c(age_group, sex, region)) %>%
      count(name, value, rotapcr)  %>%
     group_by(value) %>% 
     mutate(perc = round(n*100/sum(n),1),
            deno = sum(n),
       tot = paste0(n,"/",deno, " (", perc, ")")) %>%
     filter(rotapcr != "neg") %>%
     select(Characteristics  = value,tot) %>%
 rows_insert(tibble(Characteristics = c("[0,7)","[13,19)"),
              tot = c("0/7 (0.0)", "0/4 (0.0)"))) %>%
   arrange(match(Characteristics, c("[0,7)","[7,13)","[13,19)","[19,25)",
                 "[25,60)","F","M","rural","urban")))


tab <- left_join(tab1, tab2, by = "Characteristics") 

rotcase$rotapcr <- as.factor(rotcase$rotapcr)
dep <- "rotapcr" 
exp<- c("age_group", "sex", "region") 
res1 <- rotcase %>%
        glmuni(dep, exp) %>% 
        fit2df(estimate_suffix="",
               condense = FALSE) %>%
  mutate_at(vars(OR:p), funs(round(., 2)))


ff <- rotcont %>%
   summary_factorlist(dep, exp, cont = "median", fit_id=TRUE)%>%
  ff_merge(res1, estimate_name = "Rate ratio")  %>%
  mutate(or = paste0(OR," (",L95,"-",U95,")")) %>%
  select(label, levels,or,p) %>%
  slice(1:9)
ff$or[c(1,6,8)] <- "-"
ff$p[c(1,6,8)] <- "-"


gt <- bind_cols(tab, ff) %>%
  select(starts_with(c("c","t","o","p"))) 
 
 
```

```{r, echo=FALSE, eval=TRUE, message=FALSE, warning=FALSE}



kable(gt, col.names = NULL,longtable = T, booktabs = T,
      caption = "Prevalence of Rotavirus in diarrhoea cases and healthy children by
      age, sex, and region ") %>%
kable_styling() %>%
pack_rows(index=c("Age group(months)" = 5, "Sex" = 2,
        "Region" = 2)) %>%
add_header_above(c("Charcteristcics" = 1,
                   "Rotavirus positive/ Total \n n/N (%)" = 1,
                   "Rotavirus positive/ Total \n n/N (%)" = 1,
                   "Odds ratio(95 % CI)" = 1,
                   "P-value" = 1)) %>%
add_header_above(c(" "= 1,"Cases (N=177)" = 1, "Control (N=67)" = 1, " " = 1,
                     " " = 1))

```
\newpage
```{r echo=FALSE, message=FALSE, warning=FALSE}


tab3 <- rotcase %>%
      pivot_longer(c(age_group, sex, region,water,toilets,house,food)) %>%
      count(name, value, rotapcr)  %>%
  filter(rotapcr !="neg")%>%
     group_by(name) %>% 
     mutate(perc = round(n*100/sum(n),1),
            deno = sum(n),
       tot = paste0(n,"/",deno, " (", perc, ")")) %>%
  select(Characteristics  = value,tot) 
  

tab4 <- rotcont %>%
      pivot_longer(c(age_group, sex, region,water,toilets,house,food)) %>%
      count(value)  %>%
     mutate(perc = round(n*100/67,1),
            deno = 67,
       tot = paste0(n,"/",deno, " (", perc, ")")) %>%
     select(Characteristics  = value,tot)

tabf <-left_join(tab3, tab4, by = "Characteristics") %>%
        drop_na(Characteristics,tot.y) %>%
   arrange(match(Characteristics,  c("[0,7)","[7,13)","[13,19)","[19,25)",
                 "[25,60)","F","M","rural","urban","water piped into the house",
                "public tap","well","surface water",
            "flushing toilet","plank","cement","halfcement","normal food",
         "formula")))

rotcase$rotapcr <- as.factor(rotcase$rotapcr)
exp2 <- c("age_group", "sex", "region","water","house","toilets","food") 
res2 <- rotcase %>%
        glmuni(dep, exp2) %>% 
        fit2df(estimate_suffix="",
               condense = FALSE) %>%
  mutate_at(vars(OR:p), funs(round(., 2)))

ff2 <- rotcont %>%
   summary_factorlist(dep, exp2, cont = "median", fit_id=TRUE)%>%
  ff_merge(res2, estimate_name = "Rate ratio")  %>%
  mutate(or = paste0(OR," (",L95,"-",U95,")")) %>%
  select(label, levels,or,p) %>%
  slice(1:16)
# rotcont$rotapcr <- as.factor(rotcont$rotapcr)
# r <- rotcont %>%
#     glmuni(dep, exp2) %>%
#   fit2df(estimate_suffix = "",
#          condense = FALSE) %>%
#   mutate_at(vars(OR:p), funs(round(., 2)))
# f <- rotcase %>%
#    summary_factorlist(dep, exp2, cont = "median", fit_id=TRUE)%>%
#   ff_merge(r, estimate_name = "Rate ratio")  %>%
#   mutate(or = paste0(OR," (",L95,"-",U95,")")) %>%
#   select(label, levels,or,p) %>%
#   slice(1:23) 


gt2 <- left_join(ff2,tabf, by =  c ("levels" = "Characteristics")) %>%
  select(2,6,7,3,4) %>%
   replace(is.na(.), "-")

gt2$or[c(1,6,8,10,14)] <- "-"


kable(gt2, col.names = NULL,longtable = T, booktabs = T,
      caption = "Association of  risk factors with Rotavirus in diarrhoea") %>%
kable_styling() %>%
pack_rows(index=c("Age group(months)" = 5, "Sex" = 2,
        "Region" = 2, "Water" = 4, "House" = 3)) %>%
add_header_above(c("Charcteristcics" = 1,
                   "Rotavirus positive/ Total \n n/N (%)" = 1,
                   "Rotavirus positive/ Total \n n/N (%)" = 1,
                   "Odds ratio(95 % CI)" = 1,
                   "P-value" = 1)) %>%
add_header_above(c(" "= 1,"Cases (N=98)" = 1, "Control (N=67)" = 1, " " = 1,
                     " " = 1))

```


- Analysis performed by : Eddy Mbena

- Date of report: 2020-09-16



